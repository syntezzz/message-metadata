package com.syntezzz.textmessagemetadata.provider.parsers;

import android.support.annotation.NonNull;

import com.syntezzz.textmessagemetadata.provider.BuildConfig;
import com.syntezzz.textmessagemetadata.provider.Helpers;
import com.syntezzz.textmessagemetadata.provider.MetadataRequest;
import com.syntezzz.textmessagemetadata.provider.TestWebServerForUrlTitleMessageParser;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class UrlTitleMessageParserTest {

    private MockUrlTitleMessageParser mParser;

    @Before
    public void setUp() throws Exception {
        mParser = new MockUrlTitleMessageParser();
    }

    @Test
    public void testReturnsCorrectName() throws Exception {
        Assert.assertEquals("links", mParser.getJsonName());
    }

    @Test
    public void testPrepareUrlDoesNothing() throws Exception {
        UrlTitleMessageParser parser = new UrlTitleMessageParser();

        String url = "http://google.com";
        Assert.assertEquals(url, parser.prepareUrl(url));
    }

    @Test
    public void testParseNull() throws Exception {
        testLink(null, null);
    }

    @Test
    public void testParseEmptyString() throws Exception {
        testLink("", null);
    }

    @Test
    public void testParseStringWithouUrl() throws Exception {
        testLink("What's up bro?", null);
    }

    @Test
    public void testSingleUrl() throws Exception {
        String content = "http://google.com";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("http://google.com", "Google");

        testLink(content, expectedUrls);
    }

    @Test
    public void testSingleUrlWithoutTitle() throws Exception {
        String content = "http://notitle.com";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("http://notitle.com", null);

        testLink(content, expectedUrls);
    }

    @Test
    public void testMultipleUrls() throws Exception {
        String content = "Try http://google.com or http://notitle.com";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("http://google.com", "Google");
        expectedUrls.put("http://notitle.com", null);

        testLink(content, expectedUrls);
    }

    @Test
    public void testBrokenConnectionUrl() throws Exception {
        String content = "http://failconnection.com";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("http://failconnection.com", null);

        testLink(content, expectedUrls);
    }

    @Test
    public void testLongMultilenResponse() throws Exception {
        String content = "http://verylongresponse.com";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("http://verylongresponse.com", "Long");

        testLink(content, expectedUrls);
    }

    @Test
    public void testEmptyTitle() throws Exception {
        String content = "http://emptytitle.com";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("http://emptytitle.com", null);

        testLink(content, expectedUrls);
    }

    @Test
    public void testCancelAfterLoadingStarted() throws Exception {
        String content = "http://veryveryslowgoogle.com";

        new TestWebServerForUrlTitleMessageParser(mParser);

        MetadataRequest metadataRequest = mock(MetadataRequest.class);

        when(metadataRequest.getContent()).thenReturn(content);

        mParser.parseMessage(metadataRequest);

        // Waiting for threads to start
        Thread.sleep(2000);

        mParser.cancel(metadataRequest);

        verify(metadataRequest, never()).onParsingComplete(same(mParser), any(JSONArray.class));

        Thread.sleep(5000);

        Robolectric.flushForegroundThreadScheduler();

        verify(metadataRequest, never()).onParsingComplete(same(mParser), any(JSONArray.class));
    }

    @Test
    public void testCancelInstantly() throws Exception {
        String content = "http://veryveryslowgoogle.com";

        new TestWebServerForUrlTitleMessageParser(mParser);

        MetadataRequest metadataRequest = mock(MetadataRequest.class);

        when(metadataRequest.getContent()).thenReturn(content);

        mParser.parseMessage(metadataRequest);

        mParser.cancel(metadataRequest);

        verify(metadataRequest, never()).onParsingComplete(same(mParser), any(JSONArray.class));

        Thread.sleep(5000);

        Robolectric.flushForegroundThreadScheduler();

        verify(metadataRequest, never()).onParsingComplete(same(mParser), any(JSONArray.class));
    }

    private void testLink(String input, Map<String, String> expected)
            throws IOException, InterruptedException, JSONException {
        ArgumentCaptor<JSONArray> argument = getMetadata(input);

        Helpers.assertLinkItem(expected, argument.getValue());
    }

    @NonNull
    private ArgumentCaptor<JSONArray> getMetadata(String input) throws IOException, InterruptedException {
        new TestWebServerForUrlTitleMessageParser(mParser);

        final CountDownLatch latch = new CountDownLatch(1);

        MetadataRequest metadataRequest = mock(MetadataRequest.class);

        when(metadataRequest.getContent()).thenReturn(input);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                latch.countDown();
                return null;
            }
        }).when(metadataRequest).onParsingComplete(same(mParser), any(JSONArray.class));

        mParser.parseMessage(metadataRequest);

        while (latch.getCount() != 0) {
            Robolectric.flushForegroundThreadScheduler();
            Thread.sleep(Helpers.SLEEP_FOR_CALLBACK_TIME);
        }

        ArgumentCaptor<JSONArray> argument = ArgumentCaptor.forClass(JSONArray.class);

        verify(metadataRequest).onParsingComplete(same(mParser), argument.capture());
        return argument;
    }

}