package com.syntezzz.textmessagemetadata.provider.parsers;

import com.syntezzz.textmessagemetadata.provider.BuildConfig;
import com.syntezzz.textmessagemetadata.provider.MetadataRequest;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class CustomEmoticonMessageParserTest {

    private CustomEmoticonMessageParser mParser;

    @Before
    public void setUp() throws Exception {
        mParser = new CustomEmoticonMessageParser();
    }

    @Test
    public void testReturnsCorrectName() throws Exception {
        Assert.assertEquals("emoticons", mParser.getJsonName());
    }

    @Test
    public void testParseNull() throws Exception {
        //noinspection ConstantConditions
        testParseEmoticons(null, null);
    }

    @Test
    public void testParseEmptyString() throws Exception {
        testParseEmoticons("", null);
    }

    @Test
    public void testParseSingleEmoticon() throws Exception {
        testParseEmoticons("(coffee)", new String[]{"coffee"});
    }

    @Test
    public void testParseMultipleEmoticon() throws Exception {
        testParseEmoticons("Drink (coffee) be a (star)", new String[]{"coffee", "star"});
    }

    @Test
    public void testNotParseEmoticonOver15Chars() throws Exception {
        testParseEmoticons("(verylongemoticon)", null);
    }

    @Test
    public void testNotParseEmoticonOver15CharsAlongsideNormal() throws Exception {
        testParseEmoticons("Drink (coffee) be a (star)(verylongemoticon)",
                new String[]{"coffee", "star"});
    }

    @Test
    public void testParseEmoticon15Chars() throws Exception {
        testParseEmoticons("(15charsemoticon)", new String[]{"15charsemoticon"});
    }

    private void testParseEmoticons(String input, String[] expected) throws JSONException {
        MetadataRequest request = mock(MetadataRequest.class);
        when(request.getContent()).thenReturn(input);

        mParser.parseMessage(request);

        ArgumentCaptor<JSONArray> argumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        verify(request).onParsingComplete(same(mParser), argumentCaptor.capture());

        JSONArray result = argumentCaptor.getValue();

        if (expected == null) {
            Assert.assertNull(result);
        } else {
            Assert.assertNotNull(result);
            Assert.assertEquals(expected.length, result.length());

            for (int i = 0; i < result.length(); i++) {
                Assert.assertEquals(expected[i], result.getString(i));
            }
        }
    }

    @Test
    public void testCancelNull() throws Exception {
        //noinspection ConstantConditions
        mParser.cancel(null);
    }

    @Test
    public void testCancelSomeRequest() throws Exception {
        mParser.cancel(mock(MetadataRequest.class));
    }
}