package com.syntezzz.textmessagemetadata.provider.parsers;

import android.support.annotation.NonNull;

import junit.framework.Assert;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MockUrlTitleMessageParser extends UrlTitleMessageParser {

    private String mBaseUrl;

    public void setBaseUrl(String baseUrl) {
        mBaseUrl = baseUrl;
    }

    @NonNull
    @Override
    protected String prepareUrl(String url) {
        Assert.assertNotNull(mBaseUrl);

        try {
            return mBaseUrl + URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Assert.fail(e.toString());
        }

        return url;
    }
}
