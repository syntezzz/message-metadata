package com.syntezzz.textmessagemetadata.provider;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class MetadataProviderTest {

    private MetadataProvider mMetadataProvider;
    private OnMetadataReadyListener mMetadataReadyListener;

    @Before
    public void setUp() {
        mMetadataReadyListener = mock(OnMetadataReadyListener.class);
        mMetadataProvider = new MetadataProvider(mMetadataReadyListener);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructProviderWithNullListener() throws Exception {
        //noinspection ConstantConditions
        new MetadataProvider(new MessageParsersRegistry(), null);
    }

    @Test
    public void testRetrieveMetadataFromNull() {
        testSameContentRetrievedWithNoMetadata(null);
    }

    @Test
    public void testRetrieveMetadataFromEmptyString() throws Exception {
        testSameContentRetrievedWithNoMetadata("");
    }

    @Test
    public void testRetrieveMetadataWithSomeString() throws Exception {
        testSameContentRetrievedWithNoMetadata("Weird message");
    }

    private void testSameContentRetrievedWithNoMetadata(String content) {
        mMetadataProvider.retrieveMetadata(content);
        verify(mMetadataReadyListener).onMetadataReady(content, null);
    }

    @Test
    public void testCancelNoExistingPending() throws Exception {
        mMetadataProvider.cancelPending("WAT");
    }

    @Test
    public void testCancelNull() throws Exception {
        mMetadataProvider.cancelPending(null);
    }

    @Test
    public void testCancelAllPendingWhenThereIsNoPending() throws Exception {
        mMetadataProvider.cancelAllPending();
    }

    @Test
    public void testResubmitOfPendingRequest() throws Exception {
        // mock will not call on MetadataRequest.onParsingComplete()
        // so this will be like endless processing
        MessageParser messageParser = mock(MessageParser.class);
        MessageParsersRegistry registry = mock(MessageParsersRegistry.class);

        ArrayList<MessageParser> messageParsers = new ArrayList<>();
        messageParsers.add(messageParser);

        when(registry.getMessageParsers()).thenReturn(messageParsers);

        MetadataProvider provider =
                new MetadataProvider(registry, mock(OnMetadataReadyListener.class));

        String content = "test";

        provider.retrieveMetadata(content);

        ArgumentCaptor<MetadataRequest> metadataRequestArgumentCaptor =
                ArgumentCaptor.forClass(MetadataRequest.class);

        verify(messageParser).parseMessage(metadataRequestArgumentCaptor.capture());

        Assert.assertEquals(content, metadataRequestArgumentCaptor.getValue().getContent());

        reset(messageParser);

        provider.retrieveMetadata(content);

        verify(messageParser, never()).parseMessage(any(MetadataRequest.class));
    }

    @Test
    public void testCancelOfPendingRequest() throws Exception {
        // mock will not call on MetadataRequest.onParsingComplete()
        // so this will be like endless processing
        MessageParser messageParser = mock(MessageParser.class);
        MessageParsersRegistry registry = mock(MessageParsersRegistry.class);

        ArrayList<MessageParser> messageParsers = new ArrayList<>();
        messageParsers.add(messageParser);

        when(registry.getMessageParsers()).thenReturn(messageParsers);

        MetadataProvider provider =
                new MetadataProvider(registry, mock(OnMetadataReadyListener.class));

        String content = "test";

        provider.retrieveMetadata(content);

        ArgumentCaptor<MetadataRequest> metadataRequestArgumentCaptor =
                ArgumentCaptor.forClass(MetadataRequest.class);

        verify(messageParser).parseMessage(metadataRequestArgumentCaptor.capture());

        MetadataRequest capturedMetadataRequest = metadataRequestArgumentCaptor.getValue();
        Assert.assertEquals(content, capturedMetadataRequest.getContent());

        provider.cancelPending(content);

        verify(messageParser).cancel(capturedMetadataRequest);
    }

    @Test
    public void testCancelAllPendingRequests() throws Exception {
        // mock will not call on MetadataRequest.onParsingComplete()
        // so this will be like endless processing
        MessageParser messageParser = mock(MessageParser.class);
        MessageParsersRegistry registry = mock(MessageParsersRegistry.class);

        ArrayList<MessageParser> messageParsers = new ArrayList<>();
        messageParsers.add(messageParser);

        when(registry.getMessageParsers()).thenReturn(messageParsers);

        MetadataProvider provider =
                new MetadataProvider(registry, mock(OnMetadataReadyListener.class));

        String content = "test";

        provider.retrieveMetadata(content);

        ArgumentCaptor<MetadataRequest> metadataRequestArgumentCaptor =
                ArgumentCaptor.forClass(MetadataRequest.class);

        verify(messageParser).parseMessage(metadataRequestArgumentCaptor.capture());

        MetadataRequest capturedMetadataRequest = metadataRequestArgumentCaptor.getValue();
        Assert.assertEquals(content, capturedMetadataRequest.getContent());

        provider.cancelAllPending();

        verify(messageParser).cancel(capturedMetadataRequest);
    }
}