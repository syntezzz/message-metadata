package com.syntezzz.textmessagemetadata.provider.parsers;

import android.support.annotation.NonNull;

import com.syntezzz.textmessagemetadata.provider.BuildConfig;
import com.syntezzz.textmessagemetadata.provider.MetadataRequest;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class BaseRegexMessageParserTest {

    private TestRegexMessageParser mTestParser;

    @Before
    public void setUp() throws Exception {
        mTestParser = new TestRegexMessageParser("(\\w+)");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullRegex() throws Exception {
        new TestRegexMessageParser(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyRegex() throws Exception {
        new TestRegexMessageParser(null);
    }

    @Test
    public void testPrepareRegexLetter() throws Exception {
        String prepared = mTestParser.prepareRegex(BaseRegexMessageParser.NON_UNICODE_LETTER);
        Assert.assertEquals(BaseRegexMessageParser.UNICODE_LETTER, prepared);
    }

    @Test
    public void testParsesNonUnicodeWord() throws Exception {
        testParsesContent("syntezzz");
    }

    @Test
    public void testParsesUnicodeWord() throws Exception {
        testParsesContent("сергей");
    }

    @Test
    public void testParsesCombinedWord() throws Exception {
        testParsesContent("syntezzzсергей");
    }

    @Test
    public void testParsesCombinedWordWithDigits() throws Exception {
        testParsesContent("syntezzzсергей123");
    }

    @Test
    public void testCanFindNothing() throws Exception {
        String content = "!!!";

        MetadataRequest metadataRequest = mock(MetadataRequest.class);
        when(metadataRequest.getContent()).thenReturn(content);
        mTestParser.parseMessage(metadataRequest);

        ArgumentCaptor<JSONArray> argument = ArgumentCaptor.forClass(JSONArray.class);

        verify(metadataRequest).onParsingComplete(same(mTestParser), argument.capture());

        Assert.assertNull(argument.getValue());
    }

    @Test
    public void testCancelNullDoesNothing() throws Exception {
        //noinspection ConstantConditions
        mTestParser.cancel(null);
    }

    @Test
    public void testCancelDoesNothing() throws Exception {
        mTestParser.cancel(mock(MetadataRequest.class));
    }

    private void testParsesContent(String content) throws JSONException {
        MetadataRequest metadataRequest = mock(MetadataRequest.class);
        when(metadataRequest.getContent()).thenReturn(content);
        mTestParser.parseMessage(metadataRequest);

        ArgumentCaptor<JSONArray> argument = ArgumentCaptor.forClass(JSONArray.class);

        verify(metadataRequest).onParsingComplete(same(mTestParser), argument.capture());

        JSONArray jsonArray = argument.getValue();

        Assert.assertEquals(1, jsonArray.length());
        Assert.assertEquals(content, jsonArray.getString(0));
    }

    private static class TestRegexMessageParser extends BaseRegexMessageParser {
        protected TestRegexMessageParser(String regex) {
            super(regex);
        }

        @NonNull
        @Override
        public String getJsonName() {
            return "test";
        }
    }
}