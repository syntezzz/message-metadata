package com.syntezzz.textmessagemetadata.provider;

import android.net.UrlQuerySanitizer;
import android.support.annotation.NonNull;

import com.syntezzz.textmessagemetadata.provider.parsers.MockUrlTitleMessageParser;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

public class Helpers {
    public static final int SLEEP_FOR_CALLBACK_TIME = 10;
    public static final String FAIL_FAIL = "Fail fail";

    @NonNull
    public static ArgumentCaptor<JSONObject> getMetadataFromInputWithLinks(
            String content, MockUrlTitleMessageParser mockUrlTitleMessageParser,
            OnMetadataReadyListener metadataReadyListener,
            MetadataProvider metadataProvider)
            throws IOException, InterruptedException {

        new TestWebServerForUrlTitleMessageParser(mockUrlTitleMessageParser);

        final CountDownLatch latch = new CountDownLatch(1);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                latch.countDown();
                return null;
            }
        }).when(metadataReadyListener).onMetadataReady(anyString(), any(JSONObject.class));

        metadataProvider.retrieveMetadata(content);

        while (latch.getCount() != 0) {
            Robolectric.flushForegroundThreadScheduler();
            Thread.sleep(SLEEP_FOR_CALLBACK_TIME);
        }

        ArgumentCaptor<JSONObject> argument = ArgumentCaptor.forClass(JSONObject.class);

        verify(metadataReadyListener).onMetadataReady(same(content), argument.capture());
        return argument;
    }

    public static void assertLinkItem(Map<String, String> expectedLinks, JSONArray jsonArray)
            throws JSONException {

        if (expectedLinks == null) {
            Assert.assertNull(jsonArray);
            return;
        }

        Assert.assertEquals(expectedLinks.size(), jsonArray.length());

        for (String key : expectedLinks.keySet()) {
            boolean hasMatch = false;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject linkObject = jsonArray.getJSONObject(i);

                String fullUrl = linkObject.getString("url");
                UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(fullUrl);
                String queryUrl = sanitizer.getValue("url");

                if (!key.equals(queryUrl)) {
                    continue;
                }

                String expectedTitle = expectedLinks.get(key);
                if (expectedTitle != null) {
                    String received = linkObject.getString("title");
                    if (FAIL_FAIL.equals(received)) {
                        Assert.fail("You should register this url in MockWebServer Dispatcher");
                    }

                    if (!expectedTitle.equals(received)) {
                        continue;
                    }
                } else {
                    if (linkObject.has("title")) {
                        if (FAIL_FAIL.equals(linkObject.getString("title"))) {
                            Assert.fail("You should register this url in MockWebServer Dispatcher");
                        }
                        continue;
                    }
                }

                hasMatch = true;
                break;
            }

            Assert.assertTrue(hasMatch);
        }
    }
}
