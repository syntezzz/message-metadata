package com.syntezzz.textmessagemetadata.provider;

import com.syntezzz.textmessagemetadata.provider.parsers.CustomEmoticonMessageParser;
import com.syntezzz.textmessagemetadata.provider.parsers.MentionMessageParser;
import com.syntezzz.textmessagemetadata.provider.parsers.MockUrlTitleMessageParser;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class IntegrationTest {

    private MetadataProvider mMetadataProvider;
    private OnMetadataReadyListener mMetadataReadyListener;
    private MockUrlTitleMessageParser mMockUrlTitleMessageParser;

    @Before
    public void setUp() {
        mMetadataReadyListener = mock(OnMetadataReadyListener.class);

        MessageParsersRegistry parsersRegistry = mock(MessageParsersRegistry.class);

        List<MessageParser> parsers = new ArrayList<>();
        parsers.add(new MentionMessageParser());
        parsers.add(new CustomEmoticonMessageParser());
        mMockUrlTitleMessageParser = new MockUrlTitleMessageParser();
        parsers.add(mMockUrlTitleMessageParser);

        when(parsersRegistry.getMessageParsers()).thenReturn(parsers);

        mMetadataProvider = new MetadataProvider(parsersRegistry, mMetadataReadyListener);
    }

    @Test
    public void testRetrieveMetadataForSimpleUsername() throws Exception {
        String content = "@syntezzz";
        String[] expected = new String[]{"syntezzz"};

        testMention(content, expected);
    }

    @Test
    public void testRetrieveMetadataForSimplePhraseWithMention() throws Exception {
        String content = "@chris you around?";
        String[] expected = new String[]{"chris"};

        testMention(content, expected);
    }

    @Test
    public void testRetrieveMetadataForSimplePhraseWithMentionInTheMiddle() throws Exception {
        String content = "Hey @chris are you around?";
        String[] expected = new String[]{"chris"};

        testMention(content, expected);
    }

    @Test
    public void testRetrieveMetadataForSimplePhraseWithMentionAndEmail() throws Exception {
        String content = "@chris you around? Write me at chris@job.com";
        String[] expected = new String[]{"chris"};

        testMention(content, expected);
    }

    @Test
    public void testRetrieveMetadataForSimplePhraseWithTwoMentions() throws Exception {
        String content = "Hey @chris are you around? Call @syntezzz for an advice";
        String[] expected = new String[]{"chris", "syntezzz"};

        testMention(content, expected);
    }

    @Test
    public void testSingleEmoticon() throws Exception {
        String content = "(coffee)";
        String[] expected = new String[]{"coffee"};

        testEmoticon(content, expected);
    }

    @Test
    public void testMultipleEmoticon() throws Exception {
        String content = "Good morning! (megusta) (coffee)";
        String[] expected = new String[]{"megusta", "coffee"};

        testEmoticon(content, expected);
    }

    @Test
    public void testCombinedEmoticonWithMentions() throws Exception {
        String content = "@bob @john (success) such a cool feature";
        String[] expectedMention = new String[]{"bob", "john"};
        String[] expectedEmoticon = new String[]{"success"};

        testBothEmoticonAndMentions(content, expectedMention, expectedEmoticon);
    }

    @Test
    public void tesSingleUrl() throws Exception {
        String content = "http://google.com";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("http://google.com", "Google");

        testLink(content, expectedUrls);
    }

    @Test
    public void tesSingleUrlWithoutTitle() throws Exception {
        String content = "http://notitle.com";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("http://notitle.com", null);

        testLink(content, expectedUrls);
    }

    @Test
    public void tesSingle404UrlNoTitle() throws Exception {
        String content = "http://error404notitle.com";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("http://error404notitle.com", null);

        testLink(content, expectedUrls);
    }

    @Test
    public void tesSingle404UrlWithTitle() throws Exception {
        String content = "http://error404.com";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("http://error404.com", "Error 404");

        testLink(content, expectedUrls);
    }

    @Test
    public void tesSingleUrlInTheMiddle() throws Exception {
        String content = "Try googling this at https://google.com!";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("https://google.com", "Google");

        testLink(content, expectedUrls);
    }

    @Test
    public void tesMultipleUrls() throws Exception {
        String content = "Try googling this at https://google.com! " +
                "Or go to http://example.com/?test=1#hash";
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("https://google.com", "Google");
        expectedUrls.put("http://example.com/?test=1#hash", "Example Domain");

        testLink(content, expectedUrls);
    }

    @Test
    public void tesMentionEmoticonAndUrl() throws Exception {
        String content = "@bob @john (success) such a cool feature; https://google.com!";

        String[] expectedMention = new String[]{"bob", "john"};
        String[] expectedEmoticon = new String[]{"success"};
        Map<String, String> expectedUrls = new HashMap<>();
        expectedUrls.put("https://google.com", "Google");

        testMentionEmoticonLink(
                content, expectedMention, expectedEmoticon, expectedUrls);
    }

    private void testMention(String content, String[] expected) throws JSONException {
        mMetadataProvider.retrieveMetadata(content);

        ArgumentCaptor<JSONObject> argument = ArgumentCaptor.forClass(JSONObject.class);

        verify(mMetadataReadyListener).onMetadataReady(same(content), argument.capture());

        assertSimpleArrayItem(expected, argument, "mentions");
    }

    private void testEmoticon(String content, String[] expected) throws JSONException {
        mMetadataProvider.retrieveMetadata(content);

        ArgumentCaptor<JSONObject> argument = ArgumentCaptor.forClass(JSONObject.class);

        verify(mMetadataReadyListener).onMetadataReady(same(content), argument.capture());

        assertSimpleArrayItem(expected, argument, "emoticons");
    }

    private void testLink(String content,
                          Map<String, String> expectedLinks)
            throws JSONException, InterruptedException, IOException {

        ArgumentCaptor<JSONObject> argument =
                Helpers.getMetadataFromInputWithLinks(content,
                        mMockUrlTitleMessageParser, mMetadataReadyListener, mMetadataProvider);

        Helpers.assertLinkItem(expectedLinks, argument.getValue().getJSONArray("links"));
    }

    private void testMentionEmoticonLink(String content,
                                         String[] expectedMention,
                                         String[] expectedEmoticon,
                                         Map<String, String> expectedLinks)
            throws JSONException, InterruptedException, IOException {

        ArgumentCaptor<JSONObject> argument =
                Helpers.getMetadataFromInputWithLinks(content,
                        mMockUrlTitleMessageParser, mMetadataReadyListener, mMetadataProvider);


        assertSimpleArrayItem(expectedMention, argument, "mentions");


        assertSimpleArrayItem(expectedEmoticon, argument, "emoticons");


        Helpers.assertLinkItem(expectedLinks, argument.getValue().getJSONArray("links"));
    }

    private void testBothEmoticonAndMentions(String content,
                                             String[] expectedMention,
                                             String[] expectedEmoticon) throws JSONException {
        mMetadataProvider.retrieveMetadata(content);

        ArgumentCaptor<JSONObject> argument = ArgumentCaptor.forClass(JSONObject.class);

        verify(mMetadataReadyListener).onMetadataReady(same(content), argument.capture());

        assertSimpleArrayItem(expectedMention, argument, "mentions");

        assertSimpleArrayItem(expectedEmoticon, argument, "emoticons");

    }

    private void assertSimpleArrayItem(String[] expected,
                                       ArgumentCaptor<JSONObject> argument,
                                       String name) throws JSONException {
        JSONArray mentions = argument.getValue().getJSONArray(name);

        Assert.assertEquals(expected.length, mentions.length());

        for (int i = 0; i < mentions.length(); i++) {
            Assert.assertEquals(expected[i], mentions.get(i));
        }
    }

}
