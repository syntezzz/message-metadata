package com.syntezzz.textmessagemetadata.provider;

import android.net.UrlQuerySanitizer;

import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.mockwebserver.Dispatcher;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.RecordedRequest;
import com.syntezzz.textmessagemetadata.provider.parsers.MockUrlTitleMessageParser;

import junit.framework.Assert;

import java.io.IOException;

public class TestWebServerForUrlTitleMessageParser {

    public TestWebServerForUrlTitleMessageParser(MockUrlTitleMessageParser urlTitleMessageParser)
            throws IOException {
        MockWebServer mockWebServer = new MockWebServer();

        mockWebServer.setDispatcher(new MockWebServerDispatcher());

        mockWebServer.start();

        HttpUrl url = mockWebServer.url("/mock/?url=");
        urlTitleMessageParser.setBaseUrl(url.toString());
    }

    static class MockWebServerDispatcher extends Dispatcher {
        @Override
        public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
            UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(request.getPath());
            String queryUrl = sanitizer.getValue("url");

            if (queryUrl.startsWith("https://google.com")
                    || queryUrl.startsWith("http://google.com")) {
                return new MockResponse()
                        .setResponseCode(200)
                        .setBody("<html><body><title>Google</title></body>");
            }

            if (queryUrl.startsWith("http://example.com")) {
                return new MockResponse()
                        .setResponseCode(200)
                        .setBody("<html><body><title>Example Domain</title></body>");
            }

            if (queryUrl.startsWith("http://notitle.com")) {
                return new MockResponse()
                        .setResponseCode(200)
                        .setBody("<html><body>Super cool pics!</body>");
            }

            if (queryUrl.startsWith("http://error404.com")) {
                return new MockResponse()
                        .setResponseCode(404)
                        .setBody("<html><body><title>Error 404</title></body>");
            }

            if (queryUrl.startsWith("http://error404notitle.com")) {
                return new MockResponse()
                        .setResponseCode(404)
                        .setBody("");
            }

            if (queryUrl.startsWith("http://verylongresponse.com")) {
                return new MockResponse()
                        .setResponseCode(200)
                        .setBody("<html>\n" +
                                "<title>Long</title>\n" +
                                "\n\n<body>Whatever</body>");
            }

            if (queryUrl.startsWith("http://emptytitle.com")) {
                return new MockResponse()
                        .setResponseCode(200)
                        .setBody("<html><body><title></title>Duh</body>");
            }

            if (queryUrl.startsWith("http://failconnection.com")) {
                throw new RuntimeException("Simulating error on the server");
            }

            if (queryUrl.startsWith("http://veryveryslowgoogle.com")) {
                Thread.sleep(5000);
                return new MockResponse()
                        .setResponseCode(200)
                        .setBody("<html><body><title>Google</title></body>");
            }


            // Assert.fail() here unfortunately not failing the test
            // so we're returning title that will be checked later
            return new MockResponse().setResponseCode(200)
                    .setBody("<html><body><title>" + Helpers.FAIL_FAIL + "</title></body>");
        }
    }
}
