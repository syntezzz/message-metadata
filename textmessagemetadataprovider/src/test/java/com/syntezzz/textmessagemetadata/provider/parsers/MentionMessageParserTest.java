package com.syntezzz.textmessagemetadata.provider.parsers;

import com.syntezzz.textmessagemetadata.provider.BuildConfig;
import com.syntezzz.textmessagemetadata.provider.MetadataRequest;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class MentionMessageParserTest {

    private MentionMessageParser mParser;

    @Before
    public void setUp() throws Exception {
        mParser = new MentionMessageParser();
    }

    @Test
    public void testReturnsCorrectName() throws Exception {
        Assert.assertEquals("mentions", mParser.getJsonName());
    }

    @Test
    public void testParseNull() throws Exception {
        testParseMentions(null, null);
    }

    @Test
    public void testParseEmptyString() throws Exception {
        testParseMentions("", null);
    }

    @Test
    public void testParseSingleMention() throws Exception {
        testParseMentions("@syntezzz", new String[]{"syntezzz"});
    }

    @Test
    public void testParseSingleMentionAtTheStartOfLongString() throws Exception {
        testParseMentions("@syntezzz are you here?", new String[]{"syntezzz"});
    }

    @Test
    public void testParseSingleMentionAtTheStartOfMultilineString() throws Exception {
        testParseMentions("@syntezzz are you here?\nCan't see you online.",
                new String[]{"syntezzz"});
    }

    @Test
    public void testParseSingleMentionInTheMiddleOfMultilineString() throws Exception {
        testParseMentions("Are you here @syntezzz?\nCan't see you online.",
                new String[]{"syntezzz"});
    }

    @Test
    public void testParseDoubleMentionInTheMiddleOfMultilineString() throws Exception {
        testParseMentions("Are you here @syntezzz?\n@syntezzz can't see you online.",
                new String[]{"syntezzz", "syntezzz"});
    }

    @Test
    public void testParseDifferentMentionInTheMiddleOfMultilineString() throws Exception {
        testParseMentions("Are you here @syntezzz?\n@john can't find you.",
                new String[]{"syntezzz", "john"});
    }

    @Test
    public void testParseSingleMentionAtTheEndOfLongString() throws Exception {
        testParseMentions("Just ask @syntezzz", new String[]{"syntezzz"});
    }

    @Test
    public void testParseUnicodeMention() throws Exception {
        testParseMentions("Just ask @сергей", new String[]{"сергей"});
    }

    @Test
    public void testParseCombinedMention() throws Exception {
        testParseMentions("Just ask @SynteZZZakaСергей", new String[]{"SynteZZZakaСергей"});
    }

    private void testParseMentions(String input, String[] expected) throws JSONException {
        MetadataRequest request = mock(MetadataRequest.class);
        when(request.getContent()).thenReturn(input);

        mParser.parseMessage(request);

        ArgumentCaptor<JSONArray> argumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        verify(request).onParsingComplete(same(mParser), argumentCaptor.capture());

        JSONArray result = argumentCaptor.getValue();

        if (expected == null) {
            Assert.assertNull(result);
        } else {
            Assert.assertNotNull(result);
            Assert.assertEquals(expected.length, result.length());

            for (int i = 0; i < result.length(); i++) {
                Assert.assertEquals(expected[i], result.getString(i));
            }
        }
    }

    @Test
    public void testCancelNull() throws Exception {
        //noinspection ConstantConditions
        mParser.cancel(null);
    }

    @Test
    public void testCancelSomeRequest() throws Exception {
        mParser.cancel(mock(MetadataRequest.class));
    }
}