package com.syntezzz.textmessagemetadata.provider.parsers;

import android.support.annotation.NonNull;

public class MentionMessageParser extends BaseRegexMessageParser {

    // starting with "@", then at least one letter, then letters or digits
    private static final String sMentionRegex = "\\s@(\\w[\\w|\\d]*)";

    public MentionMessageParser() {
        super(sMentionRegex);
    }

    @NonNull
    @Override
    public String getJsonName() {
        return "mentions";
    }

    @Override
    protected String prepareContent(String content) {
        // adding space in front to simplify regex
        return " " + content;
    }
}
