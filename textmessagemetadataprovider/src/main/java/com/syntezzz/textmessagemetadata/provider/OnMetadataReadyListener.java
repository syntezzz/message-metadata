package com.syntezzz.textmessagemetadata.provider;

import android.support.annotation.Nullable;

import org.json.JSONObject;

public interface OnMetadataReadyListener {
    void onMetadataReady(@Nullable String originalContent, @Nullable JSONObject json);
}
