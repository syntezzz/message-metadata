package com.syntezzz.textmessagemetadata.provider;

import android.support.annotation.NonNull;

import com.syntezzz.textmessagemetadata.provider.parsers.CustomEmoticonMessageParser;
import com.syntezzz.textmessagemetadata.provider.parsers.MentionMessageParser;
import com.syntezzz.textmessagemetadata.provider.parsers.UrlTitleMessageParser;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.List;

public class MessageParsersRegistry {

    @NonNull private final List<MessageParser> mMessageParsers = new ArrayList<>();

    public MessageParsersRegistry() {
        mMessageParsers.add(new MentionMessageParser());
        mMessageParsers.add(new CustomEmoticonMessageParser());
        mMessageParsers.add(new UrlTitleMessageParser());
    }

    @NonNull
    public List<MessageParser> getMessageParsers() {
        Assert.assertFalse(mMessageParsers.isEmpty());
        return mMessageParsers;
    }
}
