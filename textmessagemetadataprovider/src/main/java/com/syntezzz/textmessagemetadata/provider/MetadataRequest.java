package com.syntezzz.textmessagemetadata.provider;

import android.support.annotation.NonNull;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MetadataRequest {

    @NonNull
    private final MetadataProvider mMetadataProvider;

    @NonNull
    private final List<MessageParser> mPendingParsers = new ArrayList<>();
    @NonNull
    private final Map<MessageParser, JSONArray> mResults = new HashMap<>();

    @NonNull
    private final String mContent;

    private boolean mIsProcessingStarted = false;

    public MetadataRequest(@NonNull MetadataProvider metadataProvider,
                           @NonNull MessageParsersRegistry parsersRegistry,
                           @NonNull String content) {
        mMetadataProvider = metadataProvider;
        mContent = content;

        mPendingParsers.addAll(parsersRegistry.getMessageParsers());
    }

    @NonNull
    public String getContent() {
        return mContent;
    }

    public void startProcessing() {
        Assert.assertFalse(mIsProcessingStarted);
        mIsProcessingStarted = true;

        List<MessageParser> parsers = new ArrayList<>(mPendingParsers);
        for (MessageParser parser : parsers) {
            parser.parseMessage(this);
        }
    }

    public void onParsingComplete(MessageParser messageParser, JSONArray jsonArray) {
        mPendingParsers.remove(messageParser);

        Assert.assertFalse(mResults.containsKey(messageParser));
        mResults.put(messageParser, jsonArray);

        if (mPendingParsers.isEmpty()) {
            onProcessingComplete();
        }
    }

    private void onProcessingComplete() {
        JSONObject jsonObject = new JSONObject();
        for (MessageParser parser : mResults.keySet()) {
            JSONArray array = mResults.get(parser);
            if (array != null) {
                try {
                    jsonObject.put(parser.getJsonName(), array);
                } catch (JSONException e) {
                    // no real chance that happens so just skipping this result
                }
            }
        }
        if (jsonObject.length() != 0) {
            mMetadataProvider.onRequestCompleted(this, jsonObject);
        } else {
            mMetadataProvider.onRequestCompleted(this, null);
        }
    }

    public void cancel() {
        for (MessageParser parser : mPendingParsers) {
            parser.cancel(this);
        }
    }
}
