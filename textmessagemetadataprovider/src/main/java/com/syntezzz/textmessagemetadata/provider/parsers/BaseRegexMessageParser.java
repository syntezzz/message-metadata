package com.syntezzz.textmessagemetadata.provider.parsers;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.syntezzz.textmessagemetadata.provider.MessageParser;
import com.syntezzz.textmessagemetadata.provider.MetadataRequest;

import org.json.JSONArray;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class BaseRegexMessageParser implements MessageParser {

    static final String NON_UNICODE_LETTER = "\\w";
    static final String UNICODE_LETTER =
            "[\\p{L}\\p{M}\\p{Nd}\\p{Nl}\\p{Pc}[\\p{InEnclosedAlphanumerics}&&\\p{So}]]";

    @NonNull
    protected final Pattern mPattern;

    protected BaseRegexMessageParser(@NonNull String regex) {
        if (TextUtils.isEmpty(regex)) {
            throw new IllegalArgumentException();
        }

        regex = prepareRegex(regex);
        mPattern = Pattern.compile(regex,
                Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    }

    @VisibleForTesting
    String prepareRegex(@NonNull String regex) {
        // \w and \d are not handling unicode correctly
        // see http://stackoverflow.com/a/4307261/175221 for details
        return regex.replace(NON_UNICODE_LETTER, UNICODE_LETTER);
    }

    public void parseMessage(@NonNull MetadataRequest metadataRequest) {
        String content = metadataRequest.getContent();

        JSONArray resultJsonArray = null;
        //noinspection ConstantConditions
        if (content != null) {
            resultJsonArray = findMatches(content);
        }

        metadataRequest.onParsingComplete(this, resultJsonArray);
    }

    protected String prepareContent(String content) {
        return content;
    }

    @Nullable
    private JSONArray findMatches(@NonNull String content) {
        content = prepareContent(content);

        Matcher matcher = mPattern.matcher(content);

        if (!matcher.find()) {
            return null;
        }

        JSONArray matches = new JSONArray();

        do {
            matches.put(matcher.group(1));
        } while (matcher.find());

        return matches;
    }

    public void cancel(@NonNull MetadataRequest metadataRequest) {
        // no long operations here so doing nothing
    }
}
