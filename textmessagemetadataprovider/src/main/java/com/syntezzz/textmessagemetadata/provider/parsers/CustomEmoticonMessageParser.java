package com.syntezzz.textmessagemetadata.provider.parsers;

import android.support.annotation.NonNull;

public class CustomEmoticonMessageParser extends BaseRegexMessageParser {

    private static final String sEmoticonRegex = "\\s\\((\\w{1,15})\\)";

    public CustomEmoticonMessageParser() {
        super(sEmoticonRegex);
    }

    @NonNull
    @Override
    public String getJsonName() {
        return "emoticons";
    }

    @Override
    protected String prepareContent(String content) {
        // adding space in front to simplify regex
        return " " + content;
    }
}
