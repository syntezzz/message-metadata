package com.syntezzz.textmessagemetadata.provider;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MetadataProvider {

    @NonNull
    private final OnMetadataReadyListener mOnMetadataReadyListener;

    @NonNull
    private final MessageParsersRegistry mParsersRegistry;

    @NonNull
    private Map<String, MetadataRequest> mPendingRequests = new HashMap<>();

    public MetadataProvider(@NonNull OnMetadataReadyListener onMetadataReadyListener) {
        this(new MessageParsersRegistry(), onMetadataReadyListener);
    }

    @VisibleForTesting
    public MetadataProvider(@NonNull MessageParsersRegistry messageParsersRegistry,
                            @NonNull OnMetadataReadyListener onMetadataReadyListener) {
        //noinspection ConstantConditions
        if (onMetadataReadyListener == null) {
            throw new IllegalArgumentException("Listener cannot be null");
        }
        mOnMetadataReadyListener = onMetadataReadyListener;
        mParsersRegistry = messageParsersRegistry;
    }

    public void retrieveMetadata(@Nullable String content) {
        if (TextUtils.isEmpty(content)) {
            mOnMetadataReadyListener.onMetadataReady(content, null);
            return;
        }

        if (mPendingRequests.containsKey(content)) {
            return;
        }

        MetadataRequest metadataRequest = new MetadataRequest(this, mParsersRegistry, content);

        mPendingRequests.put(content, metadataRequest);

        metadataRequest.startProcessing();
    }

    public void onRequestCompleted(@NonNull MetadataRequest metadataRequest,
                                   @Nullable JSONObject jsonObject) {
        String content = metadataRequest.getContent();
        mPendingRequests.remove(content);

        mOnMetadataReadyListener.onMetadataReady(content, jsonObject);
    }

    public void cancelPending(@Nullable String content) {
        if (TextUtils.isEmpty(content)) {
            return;
        }

        MetadataRequest metadataRequest = mPendingRequests.remove(content);
        if (metadataRequest != null) {
            metadataRequest.cancel();
        }
    }

    public void cancelAllPending() {
        for (String key : mPendingRequests.keySet()) {
            MetadataRequest metadataRequest = mPendingRequests.get(key);
            metadataRequest.cancel();
        }

        mPendingRequests.clear();
    }
}
