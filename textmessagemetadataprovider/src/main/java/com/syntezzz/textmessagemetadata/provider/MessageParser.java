package com.syntezzz.textmessagemetadata.provider;

import android.support.annotation.NonNull;

public interface MessageParser {
    @NonNull String getJsonName();
    void parseMessage(@NonNull MetadataRequest metadataRequest);
    void cancel(@NonNull MetadataRequest metadataRequest);
}
