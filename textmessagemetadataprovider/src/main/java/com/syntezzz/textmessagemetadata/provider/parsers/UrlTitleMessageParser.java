package com.syntezzz.textmessagemetadata.provider.parsers;

import android.os.Handler;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;

import com.syntezzz.textmessagemetadata.provider.MessageParser;
import com.syntezzz.textmessagemetadata.provider.MetadataRequest;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class UrlTitleMessageParser implements MessageParser {

    private static final String URL_JSON_NAME = "url";
    private static final String TITLE_JSON_NAME = "title";
    private static final String URL_REGEX =
            "\\b(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[A-Z0-9+&@#/%=~_|]";

    @NonNull
    private final ThreadFactory mThreadFactory = new NamedThreadFactory();
    @NonNull
    private final ExecutorService mExecutor = Executors.newCachedThreadPool(mThreadFactory);
    @NonNull
    private final Pattern mPattern = Pattern.compile(URL_REGEX,
            Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

    @NonNull
    private OnTitleReceivedListener mOnTitleReceivedListener = new OnTitleReceivedListener();

    @NonNull
    private final Map<MetadataRequest, List<Future>> mPendingRequests = new HashMap<>();
    @NonNull
    private final Map<MetadataRequest, List<Result>> mPendingResults = new HashMap<>();

    @NonNull
    @Override
    public String getJsonName() {
        return "links";
    }

    @Override
    public void parseMessage(@NonNull MetadataRequest metadataRequest) {
        String content = metadataRequest.getContent();

        List<String> urls = findUrls(content);

        if (urls == null) {
            metadataRequest.onParsingComplete(this, null);
            return;
        }

        List<Future> pendingDownloads = new ArrayList<>(urls.size());
        mPendingRequests.put(metadataRequest, pendingDownloads);
        mPendingResults.put(metadataRequest, new ArrayList<Result>(urls.size()));
        for (String url : urls) {
            url = prepareUrl(url);
            Future<?> future = mExecutor.submit(
                    new TitleFetcherRunnable(metadataRequest, mOnTitleReceivedListener, url));
            pendingDownloads.add(future);
        }
    }

    @VisibleForTesting
    @NonNull
    protected String prepareUrl(String url) {
        return url;
    }

    @Nullable
    private List<String> findUrls(String content) {
        if (TextUtils.isEmpty(content)) {
            return null;
        }

        Matcher matcher = mPattern.matcher(content);

        if (!matcher.find()) {
            return null;
        }

        List<String> urls = new ArrayList<>();

        do {
            urls.add(matcher.group(0));
        } while (matcher.find());

        return urls;
    }

    @Override
    public void cancel(@NonNull MetadataRequest metadataRequest) {
        List<Future> futures = mPendingRequests.get(metadataRequest);

        if (futures != null) {
            for (Future future : futures) {
                future.cancel(true);
            }
        }

        mPendingRequests.remove(metadataRequest);
        mPendingResults.remove(metadataRequest);
    }

    private static class Result {
        String url;
        String title;

        public Result(String url, String title) {
            this.url = url;
            this.title = title;
        }
    }

    private class OnTitleReceivedListener {
        @MainThread
        public void onTitleReceived(@NonNull MetadataRequest metadataRequest,
                                    @NonNull String url,
                                    @Nullable String title) {

            if (!mPendingRequests.containsKey(metadataRequest)) {
                // we were cancelled or already completed
                return;
            }

            List<Result> results = mPendingResults.get(metadataRequest);
            Assert.assertNotNull(results);

            results.add(new Result(url, title));

            List<Future> futures = mPendingRequests.get(metadataRequest);

            if (results.size() != futures.size()) {
                // still work to do
                return;
            }

            mPendingRequests.remove(metadataRequest);

            JSONArray jsonArray = new JSONArray();

            for (Result result : results) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(URL_JSON_NAME, result.url);
                    jsonObject.put(TITLE_JSON_NAME, result.title);
                    jsonArray.put(jsonObject);
                } catch (JSONException ignored) {
                    // no real chance that happens so just skipping this result
                }
            }

            if (jsonArray.length() != 0) {
                metadataRequest.onParsingComplete(UrlTitleMessageParser.this, jsonArray);
            } else {
                metadataRequest.onParsingComplete(UrlTitleMessageParser.this, null);
            }
        }
    }

    private static class TitleFetcherRunnable implements Runnable {

        private static final String END_TITLE_TAG = "</title>";
        private static final String END_HEAD_TAG = "</head>";

        @NonNull
        private final MetadataRequest mMetadataRequest;
        @NonNull
        private final String mUrl;
        @NonNull
        private final OnTitleReceivedListener mOnTitleReceivedListener;
        @NonNull
        private final Handler mHandler = new Handler();

        @MainThread
        public TitleFetcherRunnable(@NonNull MetadataRequest metadataRequest,
                                    @NonNull OnTitleReceivedListener onTitleReceivedListener,
                                    @NonNull String url) {
            mMetadataRequest = metadataRequest;
            mUrl = url;
            mOnTitleReceivedListener = onTitleReceivedListener;
        }

        @WorkerThread
        @Override
        public void run() {
            String body = downloadBody();

            final String title = parseForTitle(body);

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mOnTitleReceivedListener.onTitleReceived(mMetadataRequest, mUrl, title);
                }
            });
        }

        @Nullable
        private String downloadBody() {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(mUrl)
                    .build();

            Response response;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                return null;
            }

            String line;
            StringBuilder head = new StringBuilder();

            Reader stream = null;
            BufferedReader reader = null;
            try {
                stream = response.body().charStream();
                reader = new BufferedReader(stream);
                while ((line = reader.readLine()) != null) {
                    head.append(line);
                    if (line.contains(END_TITLE_TAG) || line.contains(END_HEAD_TAG)) {
                        break;
                    }
                }
            } catch (IOException e) {
                return null;
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException ignored) {
                    }
                }
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException ignored) {
                    }
                }
            }
            return head.toString();
        }

        @Nullable
        private String parseForTitle(String body) {
            if (TextUtils.isEmpty(body)) {
                return null;
            }

            Elements title = Jsoup.parseBodyFragment(body).getElementsByTag("title");

            if (title.isEmpty()) {
                return null;
            }

            String titleValue = title.text();

            if (TextUtils.isEmpty(titleValue)) {
                return null;
            }
            return titleValue;
        }
    }

    private static class NamedThreadFactory implements ThreadFactory {
        public static final String sNamePrefix = "url-message-parser-thread-";

        private final AtomicInteger threadNumber = new AtomicInteger(1);

        @Override
        public Thread newThread(@NonNull Runnable r) {
            return new Thread(r, sNamePrefix + threadNumber.getAndIncrement());
        }
    }
}
