package com.syntezzz.textmessagemetadata;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.TextView;

import com.syntezzz.textmessagemetadata.provider.MetadataProvider;
import com.syntezzz.textmessagemetadata.provider.OnMetadataReadyListener;

import org.json.JSONException;
import org.json.JSONObject;

public class DemoActivity extends AppCompatActivity {

    @NonNull
    private TextView mJsonTextView;
    @NonNull
    private MetadataProvider mMetadataProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        mMetadataProvider = new MetadataProvider(new MetadataReadyListener());

        setupViews();
    }

    private void setupViews() {
        TextView messageEditText = (TextView) findViewById(R.id.messageEditText);
        mJsonTextView = (TextView) findViewById(R.id.jsonTextView);

        messageEditText.addTextChangedListener(new MessageTextWatcher());
    }

    private class MetadataReadyListener implements OnMetadataReadyListener {

        private final String mErrorMessage;

        public MetadataReadyListener() {
            mErrorMessage = getString(R.string.no_metadata_retrieved_message);
        }

        @Override
        public void onMetadataReady(@Nullable String originalContent, @Nullable JSONObject json) {
            if (json == null) {
                if (TextUtils.isEmpty(originalContent)) {
                    mJsonTextView.setText("");
                } else {
                    mJsonTextView.setText(mErrorMessage);
                }
            } else {
                try {
                    mJsonTextView.setText(json.toString(4));
                } catch (JSONException e) {
                    mJsonTextView.setText(mErrorMessage);
                }
            }
        }
    }

    private class MessageTextWatcher implements TextWatcher {
        @Override
        public void afterTextChanged(Editable s) {
            mJsonTextView.setText("");
            mMetadataProvider.retrieveMetadata(s.toString());
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            mMetadataProvider.cancelPending(s.toString());
        }
    }
}
